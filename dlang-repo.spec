Name:           dlang-repo
Version:        36
Release:        %autorelease
Summary:        Extra Dlang packages

License:        CeCILL-C
URL:            none
Source0:        https://bioinfornatics.fedorapeople.org/repos/fedora/RPM-GPG-KEY-DLANG-36
Source1:        dlang.repo
BuildArch:      noarch
Requires:       fedora-release >=  %{version}

%description
This package contains the Extra Dlang packages for fedora repository
GPG key as well as configuration for dnf.

%prep
%autosetup

%install
install -Dpm 644 %{SOURCE0} \
    %{buildroot}%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-DLANG-%{version}
install -dm 755 %{buildroot}%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/yum.repos.d


%files
%license LICENSE
%config(noreplace) %{_sysconfdir}/yum.repos.d/dlang.repo

%changelog
%autochangelog
